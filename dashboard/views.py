from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseForbidden
from django.shortcuts import render
from django.views.generic import ListView,FormView
from django.db.models.base import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import MessageForm,IssueForm
from django.urls import reverse_lazy
from .models import Issue


class Dashboard(LoginRequiredMixin, ListView):
    template_name = "dashboard/dashboard.html"
    model = Issue

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


# class IssuePage(LoginRequiredMixin, DetailView):
#     model = Issue
#     template_name = 'dashboard/issue.html'


def issue_page(request, pk):
    try:
        issue = Issue.objects.get(pk=pk)
    except ObjectDoesNotExist:
        return HttpResponseForbidden()

    if issue.user != request.user:
        return HttpResponseForbidden()

    form = MessageForm()

    if request.method == "POST":
        form = MessageForm(data=request.POST)

        if form.is_valid():
            message = form.save(commit=False)
            message.user = request.user
            message.issue = issue
            message.save()

    context = {
        "issue": issue,
        "message_form": form,
    }

    return render(request, 'dashboard/issue.html', context)
#ngrok.io
class ReportPage(FormView):
    template_name = 'dashboard/report.html'
    form_class = IssueForm
    success_url = reverse_lazy('dashboard/')

    def form_valid(self, form):
        issue = form.save(commit=False)
        issue_user = self.request.user
        issue.status = 0
        issue.save()
        return super().form_valid(form)