from django import forms

from dashboard.models import Message
from .models import  Issue

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('text',)


class IssueForm(forms.ModelForm):
    text = forms.CharField(widget=forms.Textarea(attrs={'id':'editor',}))
    class Meta:
        model= Issue
        fields = '__all__'
        exclude= ('priority','user','status')
